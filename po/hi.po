# Hindi translation for app-icon-preview.
# Copyright (C) 2024 app-icon-preview's COPYRIGHT HOLDER
# This file is distributed under the same license as the app-icon-preview package.
# Scrambled777 <weblate.scrambled777@simplelogin.com>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: app-icon-preview master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/design/app-icon-"
"preview\n"
"POT-Creation-Date: 2024-03-23 14:27+0000\n"
"PO-Revision-Date: 2024-04-01 23:21+0530\n"
"Last-Translator: Scrambled777 <weblate.scrambled777@simplelogin.com>\n"
"Language-Team: Hindi\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Gtranslator 46.0\n"

#: data/org.gnome.design.AppIconPreview.desktop.in.in:3
#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:8 src/main.rs:22
#: src/widgets/project_previewer.rs:86 src/widgets/project_previewer.rs:210
msgid "App Icon Preview"
msgstr "App Icon Preview"

#: data/org.gnome.design.AppIconPreview.desktop.in.in:4
msgid "Preview applications icons"
msgstr "एप्लिकेशन आइकन का पूर्वावलोकन करें"

#: data/org.gnome.design.AppIconPreview.gschema.xml.in:6
msgid "The latest opened files separated by a comma"
msgstr "नवीनतम खोली गई फाइलें अल्पविराम से अलग की गई"

#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:9
msgid "Icon design helper"
msgstr "आइकन डिज़ाइन सहायक"

#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:11
msgid ""
"App Icon Preview is a tool for designing icons which target the GNOME "
"desktop."
msgstr ""
"ऐप आइकन पूर्वावलोकन, GNOME डेस्कटॉप को लक्षित करने वाले आइकन डिज़ाइन करने का एक उपकरण "
"है।"

#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:22
msgid "Previewing an Application icon"
msgstr "एप्लिकेशन आइकन का पूर्वावलोकन करना"

#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:114
msgid "Bilal Elmoussaoui &amp; Zander Brown"
msgstr "बिलाल एल्मौसौई &amp; जेंडर ब्राउन"

#: data/resources/ui/export.ui:16
msgid ""
"Export the icon for production use. Off-canvas objects are removed and the "
"SVG is optimised for size"
msgstr ""
"उत्पादन उपयोग के लिए आइकन निर्यात करें। ऑफ-कैनवास वस्तुएं हटा दी जाती हैं और SVG को "
"आकार के लिए अनुकूलित किया जाता है"

#: data/resources/ui/export.ui:50
msgid "Regular"
msgstr "नियमित"

#: data/resources/ui/export.ui:64
msgid "Save Regular…"
msgstr "नियमित सहेजें…"

#: data/resources/ui/export.ui:98
msgid "Nightly"
msgstr "रात्रिकालीन"

#: data/resources/ui/export.ui:112
msgid "Save Nightly…"
msgstr "रात्रिकालीन सहेजें…"

#: data/resources/ui/export.ui:148
msgid "Symbolic"
msgstr "प्रतीकात्मक"

#: data/resources/ui/export.ui:161
msgid "Save Symbolic…"
msgstr "प्रतीकात्मक सहेजें…"

#: data/resources/ui/help-overlay.ui:11
msgctxt "shortcut window"
msgid "Application"
msgstr "एप्लिकेशन"

#: data/resources/ui/help-overlay.ui:15
msgctxt "shortcut window"
msgid "New Window"
msgstr "नई विंडो"

#: data/resources/ui/help-overlay.ui:21
msgctxt "shortcut window"
msgid "Open an Icon"
msgstr "एक आइकन खोलें"

#: data/resources/ui/help-overlay.ui:27
msgctxt "shortcut window"
msgid "Quit the Application"
msgstr "एप्लिकेशन छोड़ें"

#: data/resources/ui/help-overlay.ui:34
msgctxt "shortcut window"
msgid "View"
msgstr "देखें"

#: data/resources/ui/help-overlay.ui:38
msgctxt "shortcut window"
msgid "Reload the Icon"
msgstr "आइकन पुनः लोड करें"

#: data/resources/ui/help-overlay.ui:44
msgctxt "shortcut window"
msgid "Export the Icon"
msgstr "आइकन निर्यात करें"

#: data/resources/ui/help-overlay.ui:50
msgctxt "shortcut window"
msgid "Shuffle Icons"
msgstr "आइकन को फेरबदल करें"

#: data/resources/ui/help-overlay.ui:56
msgctxt "shortcut window"
msgid "Take Screenshot"
msgstr "स्क्रीनशॉट लें"

#: data/resources/ui/help-overlay.ui:62
msgctxt "shortcut window"
msgid "Copy a Screenshot to Clipboard"
msgstr "स्क्रीनशॉट को क्लिपबोर्ड पर कॉपी करें"

#: data/resources/ui/window.ui:16
msgid "_Open"
msgstr "खोलें (_O)"

#: data/resources/ui/window.ui:18
msgid "Open an Icon"
msgstr "एक आइकन खोलें"

#: data/resources/ui/window.ui:29
msgid "Main Menu"
msgstr "मुख्य मेनू"

#: data/resources/ui/window.ui:36
msgid "_Export"
msgstr "निर्यात (_E)"

#: data/resources/ui/window.ui:54
msgid "Make a new App Icon"
msgstr "नया ऐप आइकन बनाएं"

#: data/resources/ui/window.ui:180
msgid "_New App Icon"
msgstr "नया ऐप आइकन (_N)"

#: data/resources/ui/window.ui:201
msgid "_New Window"
msgstr "नई विंडो (_N)"

#: data/resources/ui/window.ui:207
msgid "_Reload"
msgstr "पुनः लोड करें (_R)"

#: data/resources/ui/window.ui:211
msgid "_Copy Screenshot"
msgstr "स्क्रीनशॉट कॉपी करें (_C)"

#: data/resources/ui/window.ui:215
msgid "_Save Screenshot"
msgstr "स्क्रीनशॉट सहेजें (_S)"

#: data/resources/ui/window.ui:219
msgid "_Shuffle Example Icons"
msgstr "उदाहरण आइकन को फेरबदल करें (_S)"

#: data/resources/ui/window.ui:225
msgid "_Keyboard Shortcuts"
msgstr "कीबोर्ड शॉर्टकट (_K)"

#: data/resources/ui/window.ui:229
msgid "_About App Icon Preview"
msgstr "App Icon Preview के बारे में (_A)"

#: src/application.rs:112
msgid "translator-credits"
msgstr "Scrambled777 <weblate.scrambled777@simplelogin.com>, 2024"

#: src/project.rs:180
msgid "SVG"
msgstr "SVG"

#: src/project.rs:187
msgid "Export"
msgstr "निर्यात"

#: src/project.rs:188 src/widgets/project_previewer.rs:227
msgid "_Save"
msgstr "सहेजें (_S)"

#: src/widgets/project_previewer.rs:195
msgid "Screenshot copied to clipboard"
msgstr "स्क्रीनशॉट को क्लिपबोर्ड पर कॉपी किया गया"

#: src/widgets/project_previewer.rs:219
msgid "PNG"
msgstr "PNG"

#: src/widgets/project_previewer.rs:225
msgid "Save Screenshot"
msgstr "स्क्रीनशॉट सहेजें"

#: src/widgets/project_previewer.rs:228
msgid "Preview"
msgstr "पूर्वावलोकन"

#: src/widgets/window.rs:213
msgid "_Create"
msgstr "बनाएं (_C)"

#: src/widgets/window.rs:216
msgid "Select a file"
msgstr "एक फाइल चुनें"

#: src/widgets/window.rs:227
msgid "SVG images"
msgstr "SVG छवियाँ"

#: src/widgets/window.rs:232
msgid "Open File"
msgstr "फाइल खोलें"
