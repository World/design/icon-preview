# Ukrainian translation for app-icon-preview.
# Copyright (C) 2020 app-icon-preview's COPYRIGHT HOLDER
# This file is distributed under the same license as the app-icon-preview package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2020, 2021, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: app-icon-preview master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/design/app-icon-preview"
"\n"
"POT-Creation-Date: 2024-02-24 00:36+0000\n"
"PO-Revision-Date: 2024-02-24 08:49+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 23.04.1\n"

#: data/org.gnome.design.AppIconPreview.desktop.in.in:3
#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:8 src/main.rs:22
#: src/widgets/project_previewer.rs:86 src/widgets/project_previewer.rs:210
msgid "App Icon Preview"
msgstr "Перегляд піктограм програми"

#: data/org.gnome.design.AppIconPreview.desktop.in.in:4
msgid "Preview applications icons"
msgstr "Перегляд піктограм програм"

#: data/org.gnome.design.AppIconPreview.gschema.xml.in:6
msgid "The latest opened files separated by a comma"
msgstr "Останні відкриті файли, записи відокремлено комами"

#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:9
msgid "Icon design helper"
msgstr "Помічник у створенні піктограм"

#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:11
msgid ""
"App Icon Preview is a tool for designing icons which target the GNOME "
"desktop."
msgstr ""
"«Перегляд піктограм програм» — програма для розробки піктограм, яку "
"призначено для стільничного середовища GNOME."

#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:22
msgid "Previewing an Application icon"
msgstr "Попередній перегляд піктограми програми"

#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:114
msgid "Bilal Elmoussaoui &amp; Zander Brown"
msgstr "Bilal Elmoussaoui та Zander Brown"

#: data/resources/ui/export.ui:16
msgid ""
"Export the icon for production use. Off-canvas objects are removed and the "
"SVG is optimised for size"
msgstr ""
"Експортувати піктограму для використання. Об'єкти поза полотном буде "
"вилучено, а SVG буде оптимізовано за розміром."

#: data/resources/ui/export.ui:50
msgid "Regular"
msgstr "Звичайна"

#: data/resources/ui/export.ui:64
msgid "Save Regular…"
msgstr "Зберегти звичайну…"

#: data/resources/ui/export.ui:98
msgid "Nightly"
msgstr "Нічна"

#: data/resources/ui/export.ui:112
msgid "Save Nightly…"
msgstr "Зберегти нічну…"

#: data/resources/ui/export.ui:148
msgid "Symbolic"
msgstr "Символічна"

#: data/resources/ui/export.ui:161
msgid "Save Symbolic…"
msgstr "Зберегти символічну…"

#: data/resources/ui/help-overlay.ui:11
msgctxt "shortcut window"
msgid "Application"
msgstr "Програма"

#: data/resources/ui/help-overlay.ui:15
msgctxt "shortcut window"
msgid "New Window"
msgstr "Нове вікно"

#: data/resources/ui/help-overlay.ui:21
msgctxt "shortcut window"
msgid "Open an Icon"
msgstr "Відкрити піктограму"

#: data/resources/ui/help-overlay.ui:27
msgctxt "shortcut window"
msgid "Quit the Application"
msgstr "Вийти з програми"

#: data/resources/ui/help-overlay.ui:34
msgctxt "shortcut window"
msgid "View"
msgstr "Перегляд"

#: data/resources/ui/help-overlay.ui:38
msgctxt "shortcut window"
msgid "Reload the Icon"
msgstr "Перезавантажити піктограму"

#: data/resources/ui/help-overlay.ui:44
msgctxt "shortcut window"
msgid "Export the Icon"
msgstr "Експортувати піктограму"

#: data/resources/ui/help-overlay.ui:50
msgctxt "shortcut window"
msgid "Shuffle Icons"
msgstr "Перемішати піктограми"

#: data/resources/ui/help-overlay.ui:56
msgctxt "shortcut window"
msgid "Take Screenshot"
msgstr "Зробити знімок"

#: data/resources/ui/help-overlay.ui:62
msgctxt "shortcut window"
msgid "Copy a Screenshot to Clipboard"
msgstr "Копіювати знімок до буфера"

#: data/resources/ui/window.ui:16
msgid "_Open"
msgstr "_Відкрити"

#: data/resources/ui/window.ui:18
msgid "Open an Icon"
msgstr "Відкрити піктограму"

#: data/resources/ui/window.ui:29
msgid "Main Menu"
msgstr "Головне меню"

#: data/resources/ui/window.ui:36
msgid "_Export"
msgstr "_Експортувати"

#: data/resources/ui/window.ui:54
msgid "Make a new App Icon"
msgstr "Створити піктограму програми"

#: data/resources/ui/window.ui:180
msgid "_New App Icon"
msgstr "_Нова піктограма програми"

#: data/resources/ui/window.ui:201
msgid "_New Window"
msgstr "_Нове вікно"

#: data/resources/ui/window.ui:207
msgid "_Reload"
msgstr "Пе_резавантажити"

#: data/resources/ui/window.ui:211
msgid "_Copy Screenshot"
msgstr "_Копіювати знімок"

#: data/resources/ui/window.ui:215
msgid "_Save Screenshot"
msgstr "З_берегти знімок"

#: data/resources/ui/window.ui:219
msgid "_Shuffle Example Icons"
msgstr "П_еремішати приклади піктограм"

#: data/resources/ui/window.ui:225
msgid "_Keyboard Shortcuts"
msgstr "_Клавіатурні скорочення"

#: data/resources/ui/window.ui:229
msgid "_About App Icon Preview"
msgstr "_Про «Перегляд піктограм програми»"

#: src/application.rs:112
msgid "translator-credits"
msgstr "Юрій Чорноіван <yurchor@ukr.net>"

#: src/project.rs:180
msgid "SVG"
msgstr "SVG"

#: src/project.rs:187
msgid "Export"
msgstr "Експортувати"

#: src/project.rs:188 src/widgets/project_previewer.rs:227
msgid "_Save"
msgstr "З_берегти"

#: src/widgets/project_previewer.rs:195
msgid "Screenshot copied to clipboard"
msgstr "Знімок вікна скопійовано до буфера"

#: src/widgets/project_previewer.rs:219
msgid "PNG"
msgstr "PNG"

#: src/widgets/project_previewer.rs:225
msgid "Save Screenshot"
msgstr "Зберегти знімок екрана"

#: src/widgets/project_previewer.rs:228
msgid "Preview"
msgstr "Перегляд"

#: src/widgets/window.rs:213
msgid "_Create"
msgstr "С_творити"

#: src/widgets/window.rs:216
#| msgid "Select"
msgid "Select a file"
msgstr "Виберіть файл"

#: src/widgets/window.rs:227
msgid "SVG images"
msgstr "зображення SVG"

#: src/widgets/window.rs:232
msgid "Open File"
msgstr "Відкрити файл"

#~ msgid "New App Icon"
#~ msgstr "Нова піктограма програми"

#~ msgid "_Cancel"
#~ msgstr "_Скасувати"

#~ msgid "App Name"
#~ msgstr "Назва програми"

#~ msgid "Icon Location"
#~ msgstr "Розташування піктограми"

#~ msgid "The reverse domain notation name, e.g. org.inkscape.Inkscape"
#~ msgstr "Назва в оберненому записі доменів. Приклад: org.inkscape.Inkscape"

#~ msgid "The icon SVG file will be stored in this directory"
#~ msgstr "Файл піктограми у форматі SVG буде збережено до цього каталогу"

#~ msgid "~/Projects"
#~ msgstr "~/Проєкти"

#~ msgid "_Browse…"
#~ msgstr "Ви_брати…"

#~ msgid "Select Icon Location"
#~ msgstr "Виберіть розташування піктограми"

#~ msgid "Cancel"
#~ msgstr "Скасувати"

#~ msgid "Tool for designing applications icons"
#~ msgstr "Інструмент для створення піктограм програм"

#~ msgid "Open an icon"
#~ msgstr "Відкрити піктограму"

#~ msgid "JPEG"
#~ msgstr "JPEG"

#~ msgid "Default window width"
#~ msgstr "Типова ширина вікна програми"

#~ msgid "Default window height"
#~ msgstr "Типова висота вікна програми"

#~ msgid "Default window maximized behaviour"
#~ msgstr "Типова поведінка щодо максимізації вікна"

#~ msgid "_Save…"
#~ msgstr "_Зберегти…"

#~ msgid "_Copy to Clipboard"
#~ msgstr "Ск_опіювати до буфера"

#~ msgid "Recent"
#~ msgstr "Нещодавні"

#~ msgctxt "shortcut window"
#~ msgid "Open the recent list"
#~ msgstr "Відкрити список нещодавніх"

#~ msgctxt "shortcut window"
#~ msgid "Open the menu"
#~ msgstr "Відкрити меню"

#~ msgctxt "shortcut window"
#~ msgid "Take screenshot"
#~ msgstr "Зробити знімок вікна"

#~ msgid "Close"
#~ msgstr "Закрити"

#~ msgid "Open"
#~ msgstr "Відкрити"

#~ msgid "Zander Brown"
#~ msgstr "Zander Brown"

#~ msgid "Copyright © 2018-19 Zander Brown"
#~ msgstr "© Zander Brown, 2018–2019"

#~ msgid "Repository"
#~ msgstr "Сховище"

#~ msgid "Kept sane by"
#~ msgstr "Збережено у здоровому глузді"

#~ msgctxt "shortcut window"
#~ msgid "Toggle fullscreen"
#~ msgstr "Увімкнути/вимкнути повноекранний режим"

#~ msgid "no longer supported"
#~ msgstr "більше не підтримується"

#~ msgid ""
#~ "Palette is all grown up!\n"
#~ "It’s now available separately as org.gnome.zbrown.Palette"
#~ msgstr ""
#~ "Палітра розрослася!\n"
#~ "Тепер вона живе окремо як org.gnome.zbrown.Palette"

#~ msgid "Failed to save screenshot"
#~ msgstr "Не вдалося зберегти знімок вікна"

#~ msgid "This file is defective"
#~ msgstr "Цей файл пошкоджено"

#~ msgid ""
#~ "Please start from a template to ensure that your file will work as a "
#~ "GNOME icon"
#~ msgstr ""
#~ "Будь ласка, розпочніть з шаблона, щоб забезпечити можливість використання "
#~ "вашого файла як піктограми GNOME"

#~ msgid "Icons"
#~ msgstr "Піктограми"

#~ msgid "Save Regular"
#~ msgstr "Зберегти звичайну"

#~ msgid "Save Symbolic"
#~ msgstr "Зберегти символічну"

#~ msgid "Save Nightly"
#~ msgstr "ЗБерегти нічну"

#~ msgid "Icon"
#~ msgstr "Піктограма"

#~ msgid "Failed to save exported file"
#~ msgstr "Не вдалося зберегти експортований файл"

#~ msgid "Save Icon"
#~ msgstr "Зберегти піктограму"

#~ msgid "Expecting at least one “.”"
#~ msgstr "Мало бути вказано хоча б одну крапку («.»)"
